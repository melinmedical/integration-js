import {
  HttpClient,
  HttpSession,
  IAuthenticateResponse,
  config as sytemIntegrationConfig,
  ICredentials,
  sessionFactory,
} from "@psky/system-integration";
import axios, { AxiosRequestConfig } from "axios";
import { IConfig } from "./types";

class LoginService {
  private _config: IConfig;
  private partnerGatewayAxiosConfig: AxiosRequestConfig;

  public get config() {
    return this._config;
  }

  public initialize = (config: IConfig) => {
    this._config = config;
    this.partnerGatewayAxiosConfig = {
      baseURL: this._config.partnerGatewayUrl,
    };
    sytemIntegrationConfig.initialize({
      partnerGatewayUrl: this._config.partnerGatewayUrl,
    });
    sessionFactory.initialize(config);
  };

  public handleLogin = async (
    sessionId: string,
    credentials: ICredentials,
    onSessionRefreshTokenCreated: (sessionRefreshToken: string) => void
  ): Promise<HttpSession> => {
    return sessionFactory.authenticateWithSession(
      sessionId,
      credentials,
      onSessionRefreshTokenCreated
    );
  };
}

const loginService = new LoginService();

export { loginService };
