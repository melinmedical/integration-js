import React, { Component } from "react";
import { Dimensions, Text, View, ViewStyle } from "react-native";
import WebView from "react-native-webview";

import { loginService } from "./loginService";

interface IProps {
  onError: (data: any) => void;
  onSuccess: (session: any) => void;
}

interface IState extends Readonly<typeof initialState> {}

const initialState = {
  height: 0,
  activating: false,
};

class LoginView extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = initialState;
  }

  public render() {
    const webviewStyle: ViewStyle = {
      overflow: "visible",
      width: Dimensions.get("screen").width,
      height: Dimensions.get("screen").height - 200,
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      margin: "auto",
    };

    if (this.state.activating) {
      return (
        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <View style={{ height: 150 }}>
            <Text>Loading</Text>
            {/* {Helpers.Loading({
              label: translator.t("activate_activating"),
              color: "#fff",
            })} */}
          </View>
        </View>
      );
    }

    return (
      <WebView
        //   ref={WEBVIEW_REF}
        style={webviewStyle}
        source={{ uri: this.buildUrl() }}
        automaticallyAdjustContentInsets={false}
        onNavigationStateChange={this.onNavigationStateChange}
        startInLoadingState={false}
      />
    );
  }

  private buildUrl = () => {
    // const loginDomain = "https://development-portal-fe.gel.camp";
    const loginDomain = loginService.config.portalUrl;
    // const redirectUrl = "https://development-portal-fe.gel.camp";

    const redirectUrl = `${loginService.config.portalUrl}/no/login?mobile=1&version&loginRedirect=1`;

    return `${
      loginService.config.loginServiceUrl
    }?identityBaseUrl=${encodeURIComponent(
      loginService.config.identityUrl
    )}&countryCode=${loginService.config.countryCode}&loginMode=${
      loginService.config.loginMode
    }&clientId=${encodeURIComponent(
      loginService.config.clientId
    )}&loginDomain=${encodeURIComponent(
      loginDomain
    )}&redirectUrl=${encodeURIComponent(redirectUrl)}`;
    // return `https://development-platform-login-service-fe.gel.camp/?identityBaseUrl=https%3A%2F%2Fdevelopment-identity.gel.camp&countryCode=no&loginMode=patientApp&clientId=app&loginDomain=https%3A%2F%2Fdevelopment-portal.gel.camp&redirectUrl=https%3A%2F%2Fdevelopment-portal.gel.camp%2Fno%2Flogin%3Fmobile%3D1%26version%26loginRedirect%3D1`;
    // return `https://psno-platform-login-service-fe.svc.pasientsky.no/?identityBaseUrl=https%3A%2F%2Fpsno-identity.svc.pasientsky.no&countryCode=no&loginMode=patientApp&clientId=app&loginDomain=https%3A%2F%2Fpsno-portal.svc.pasientsky.no&redirectUrl=https%3A%2F%2Fpsno-portal.svc.pasientsky.no%2Fno%2Flogin%3Fmobile%3D1%26version%26loginRedirect%3D1`;
  };

  private onNavigationStateChange = async (change: { url: string }) => {
    try {
      if (change.url.includes("/success")) {
        this.setState({
          activating: true,
        });

        const parts = change.url.split("/");
        const sessionId = parts[parts.length - 1];

        this.props.onSuccess(sessionId);
      }
    } catch (e) {
      this.props.onError(e);
    }
  };
}

export { LoginView };
