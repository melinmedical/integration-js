export interface IConfig {
  partnerGatewayUrl: string;
  identityUrl: string;
  loginServiceUrl: string;
  portalUrl: string;
  loginMode: "servicePractitioner" | "patientApp";
  clientId: "cp" | "app";
  countryCode: "NO" | "FI";
  onSessionExpired?: () => void;
  getSessionRefreshToken?: () => null | string;
}
