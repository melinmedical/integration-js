# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.73](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.72...@psky/signin-integration-rn@0.0.73) (2021-08-25)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.72](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.71...@psky/signin-integration-rn@0.0.72) (2021-08-25)


### Bug Fixes

* loginType type ([301950c](https://bitbucket.org/melinmedical/integration-js/commits/301950c0612c1ce02a51c95eedf09bab2acce18d))





## [0.0.71](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.70...@psky/signin-integration-rn@0.0.71) (2021-08-10)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.70](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.69...@psky/signin-integration-rn@0.0.70) (2021-08-10)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.69](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.68...@psky/signin-integration-rn@0.0.69) (2021-08-09)


### Bug Fixes

* initilize session factory ([7de4e29](https://bitbucket.org/melinmedical/integration-js/commits/7de4e294391eb52f192440a84c92b39b47f62438))





## [0.0.68](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.67...@psky/signin-integration-rn@0.0.68) (2021-08-09)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.67](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.66...@psky/signin-integration-rn@0.0.67) (2021-08-09)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.66](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.65...@psky/signin-integration-rn@0.0.66) (2021-08-09)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.65](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.64...@psky/signin-integration-rn@0.0.65) (2021-08-09)


### Bug Fixes

* sessionid get ([b0fc11d](https://bitbucket.org/melinmedical/integration-js/commits/b0fc11d1d207a3df0c821e0277ae46ff355805c9))





## [0.0.64](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.63...@psky/signin-integration-rn@0.0.64) (2021-08-09)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.63](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.62...@psky/signin-integration-rn@0.0.63) (2021-08-09)


### Bug Fixes

* return sessionId ([34356d1](https://bitbucket.org/melinmedical/integration-js/commits/34356d1d9dc274d640f0a168a4fd5486a58e6449))
* update login service ([878520d](https://bitbucket.org/melinmedical/integration-js/commits/878520d6838b75822103685430c5ea78d93d9931))





## [0.0.62](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.61...@psky/signin-integration-rn@0.0.62) (2021-08-09)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.61](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.60...@psky/signin-integration-rn@0.0.61) (2021-08-09)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.60](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.59...@psky/signin-integration-rn@0.0.60) (2021-08-09)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.59](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.58...@psky/signin-integration-rn@0.0.59) (2021-08-09)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.58](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.57...@psky/signin-integration-rn@0.0.58) (2021-08-05)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.57](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.56...@psky/signin-integration-rn@0.0.57) (2021-08-05)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.56](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.55...@psky/signin-integration-rn@0.0.56) (2021-07-27)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.55](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.54...@psky/signin-integration-rn@0.0.55) (2021-07-26)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.54](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.53...@psky/signin-integration-rn@0.0.54) (2021-07-26)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.53](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.52...@psky/signin-integration-rn@0.0.53) (2021-07-23)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.52](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.51...@psky/signin-integration-rn@0.0.52) (2021-07-23)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.51](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.50...@psky/signin-integration-rn@0.0.51) (2021-07-15)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.50](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.49...@psky/signin-integration-rn@0.0.50) (2021-07-15)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.49](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.48...@psky/signin-integration-rn@0.0.49) (2021-07-15)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.48](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.47...@psky/signin-integration-rn@0.0.48) (2021-07-14)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.47](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.46...@psky/signin-integration-rn@0.0.47) (2021-07-14)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.46](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.45...@psky/signin-integration-rn@0.0.46) (2021-07-14)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.45](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.44...@psky/signin-integration-rn@0.0.45) (2021-07-14)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.44](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.43...@psky/signin-integration-rn@0.0.44) (2021-07-12)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.43](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.42...@psky/signin-integration-rn@0.0.43) (2021-07-12)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.42](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.41...@psky/signin-integration-rn@0.0.42) (2021-07-12)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.41](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.40...@psky/signin-integration-rn@0.0.41) (2021-07-12)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.40](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.39...@psky/signin-integration-rn@0.0.40) (2021-07-09)


### Bug Fixes

* rm apikey ([bb2c245](https://bitbucket.org/melinmedical/integration-js/commits/bb2c2452da1e1402d469872546f9a7b695c3478e))





## [0.0.39](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.38...@psky/signin-integration-rn@0.0.39) (2021-07-09)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.38](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.37...@psky/signin-integration-rn@0.0.38) (2021-07-09)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.37](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.36...@psky/signin-integration-rn@0.0.37) (2021-07-08)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.36](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.35...@psky/signin-integration-rn@0.0.36) (2021-07-08)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.35](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.34...@psky/signin-integration-rn@0.0.35) (2021-07-08)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.34](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.33...@psky/signin-integration-rn@0.0.34) (2021-07-08)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.33](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.32...@psky/signin-integration-rn@0.0.33) (2021-07-08)

**Note:** Version bump only for package @psky/signin-integration-rn





## [0.0.32](https://bitbucket.org/melinmedical/integration-js/compare/@psky/signin-integration-rn@0.0.31...@psky/signin-integration-rn@0.0.32) (2021-07-05)

**Note:** Version bump only for package @psky/signin-integration-rn





## 0.0.31 (2021-07-05)

**Note:** Version bump only for package @psky/signin-integration-rn
