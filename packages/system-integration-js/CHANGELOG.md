# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.75](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.74...@psky/system-integration@0.0.75) (2021-08-25)

**Note:** Version bump only for package @psky/system-integration





## [0.0.74](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.73...@psky/system-integration@0.0.74) (2021-08-25)


### Bug Fixes

* big ([98644e0](https://bitbucket.org/melinmedical/integration-js/commits/98644e0890d3f5ef497f2bb7619fb7df4dd46645))





## [0.0.73](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.72...@psky/system-integration@0.0.73) (2021-08-25)


### Bug Fixes

* test ([9583a85](https://bitbucket.org/melinmedical/integration-js/commits/9583a852893ae7b9dfcdea8a21f9db9bfbbc331e))
* test build ([c894936](https://bitbucket.org/melinmedical/integration-js/commits/c89493665fb6fd7518985c3af435d0d50d581f65))





## [0.0.72](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.71...@psky/system-integration@0.0.72) (2021-08-25)


### Bug Fixes

*  build test ([571cbff](https://bitbucket.org/melinmedical/integration-js/commits/571cbffa674bd40028168c3088e997267e4a9c4a))





## [0.0.71](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.70...@psky/system-integration@0.0.71) (2021-08-25)


### Bug Fixes

* test build ([e4afcc7](https://bitbucket.org/melinmedical/integration-js/commits/e4afcc743ae51ea5818111837b58c51ce4aa07f3))





## [0.0.70](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.69...@psky/system-integration@0.0.70) (2021-08-25)

**Note:** Version bump only for package @psky/system-integration





## [0.0.69](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.68...@psky/system-integration@0.0.69) (2021-08-25)


### Bug Fixes

* test build ([168db86](https://bitbucket.org/melinmedical/integration-js/commits/168db86d3abd230c1992dddc1e5e7af7a6a91c3c))





## [0.0.68](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.67...@psky/system-integration@0.0.68) (2021-08-25)


### Bug Fixes

* test build ([4662783](https://bitbucket.org/melinmedical/integration-js/commits/466278384bf79afe0cabb02b68f02df22faeace1))
* test build ([88db66b](https://bitbucket.org/melinmedical/integration-js/commits/88db66b78e9ffe88630edb50972ce1e425544a4e))





## [0.0.67](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.66...@psky/system-integration@0.0.67) (2021-08-25)


### Bug Fixes

* test build ([ab1bc6f](https://bitbucket.org/melinmedical/integration-js/commits/ab1bc6f63d4a4c03f910bd48b79714743888198f))





## [0.0.66](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.65...@psky/system-integration@0.0.66) (2021-08-25)


### Bug Fixes

* test deployment flow ([59269c7](https://bitbucket.org/melinmedical/integration-js/commits/59269c75305a17a8660fef1847b5b8c0ab613790))
* test release flow ([befd3d0](https://bitbucket.org/melinmedical/integration-js/commits/befd3d07b4207e87a741addaf79b41911460577a))





## [0.0.65](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.64...@psky/system-integration@0.0.65) (2021-08-10)

**Note:** Version bump only for package @psky/system-integration





## [0.0.64](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.63...@psky/system-integration@0.0.64) (2021-08-09)

**Note:** Version bump only for package @psky/system-integration





## [0.0.63](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.62...@psky/system-integration@0.0.63) (2021-08-09)

**Note:** Version bump only for package @psky/system-integration





## [0.0.62](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.62...@psky/system-integration@0.0.62) (2021-08-09)

**Note:** Version bump only for package @psky/system-integration





## [0.0.62](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.61...@psky/system-integration@0.0.62) (2021-08-09)


### Bug Fixes

* bump version ([372ffd9](https://bitbucket.org/melinmedical/integration-js/commits/372ffd9cc8350e44ed5a571b53b4108fd4006689))





## [0.0.61](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.60...@psky/system-integration@0.0.61) (2021-08-09)


### Bug Fixes

* bump version ([6adcca8](https://bitbucket.org/melinmedical/integration-js/commits/6adcca823efe03faefb06a8243469f1514f2f585))





## [0.0.60](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.59...@psky/system-integration@0.0.60) (2021-08-09)


### Bug Fixes

* force version bump ([7dfc5f5](https://bitbucket.org/melinmedical/integration-js/commits/7dfc5f52755a71f1b45a37227f2eda295e9a94b5))





## [0.0.59](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.58...@psky/system-integration@0.0.59) (2021-08-09)


### Bug Fixes

* bad import fix ([82eb982](https://bitbucket.org/melinmedical/integration-js/commits/82eb9826bb17fc5b0ca96ad0369cd9def4d64945))
* bad import fix ([f1b2de3](https://bitbucket.org/melinmedical/integration-js/commits/f1b2de362e2e60be7a9280e5c118ec47a0f1be94))





## [0.0.58](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.57...@psky/system-integration@0.0.58) (2021-08-09)


### Bug Fixes

* pass carriers ([9138e1d](https://bitbucket.org/melinmedical/integration-js/commits/9138e1d5de786b741e1cdf463f38456bca089cd9))





## [0.0.57](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.56...@psky/system-integration@0.0.57) (2021-08-05)


### Bug Fixes

* updated error codes ([ca7ddc3](https://bitbucket.org/melinmedical/integration-js/commits/ca7ddc3800b8d31d401633ce9ab358195b126917))





## [0.0.56](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.55...@psky/system-integration@0.0.56) (2021-08-05)


### Bug Fixes

* add  null check ([c087b62](https://bitbucket.org/melinmedical/integration-js/commits/c087b621f67da544f081695334c12cc04aba62df))
* add support for data carriers ([ec3933c](https://bitbucket.org/melinmedical/integration-js/commits/ec3933c8e25617d4e39bc0b2b09e54be2eb1aefa))
* bits to rebuild context and session ([4104eea](https://bitbucket.org/melinmedical/integration-js/commits/4104eea89f2d6b36f04e5ef56dfc027afd5abb4b))
* fix copy pasta ([56ac24e](https://bitbucket.org/melinmedical/integration-js/commits/56ac24e527bf2f21c64563b18ef9ed644ad36457))
* move stuff around ([463a3e6](https://bitbucket.org/melinmedical/integration-js/commits/463a3e69d84fb4233b6a30e23ca1fe43abe4f8e6))
* refresh token logic fixed ([a68e06c](https://bitbucket.org/melinmedical/integration-js/commits/a68e06ced8573eba326f4a7d1f0d3648cc96e2ca))
* remove redundant stuff ([253542a](https://bitbucket.org/melinmedical/integration-js/commits/253542ae5360a150bfc5a210e3dc05d9c417fa32))





## [0.0.55](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.54...@psky/system-integration@0.0.55) (2021-07-27)


### Bug Fixes

* handle session and context expiry cases ([15a0e97](https://bitbucket.org/melinmedical/integration-js/commits/15a0e9740dc4aef3aa89e5784eb7c990275db346))





## [0.0.54](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.53...@psky/system-integration@0.0.54) (2021-07-26)


### Bug Fixes

* get context in response obj ([e2bcfca](https://bitbucket.org/melinmedical/integration-js/commits/e2bcfcaa263f5efb3f58a815314c8f04f8e1edd9))





## [0.0.53](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.52...@psky/system-integration@0.0.53) (2021-07-26)


### Bug Fixes

* update response contract ([2dac49f](https://bitbucket.org/melinmedical/integration-js/commits/2dac49f213676c9c850c3bcdc456cc3a40196f0a))





## [0.0.52](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.51...@psky/system-integration@0.0.52) (2021-07-23)

**Note:** Version bump only for package @psky/system-integration





## [0.0.51](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.50...@psky/system-integration@0.0.51) (2021-07-23)


### Bug Fixes

* use response type for responses ([977edfa](https://bitbucket.org/melinmedical/integration-js/commits/977edfafe2c2cccb3261a8bddac6bf848c96e327))





## [0.0.50](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.49...@psky/system-integration@0.0.50) (2021-07-15)

**Note:** Version bump only for package @psky/system-integration





## [0.0.49](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.48...@psky/system-integration@0.0.49) (2021-07-15)

**Note:** Version bump only for package @psky/system-integration





## [0.0.48](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.47...@psky/system-integration@0.0.48) (2021-07-15)


### Bug Fixes

* dont pass api key if there's none ([77455d3](https://bitbucket.org/melinmedical/integration-js/commits/77455d3213083f1d9f99882d6c34598dd45240dc))





## [0.0.47](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.46...@psky/system-integration@0.0.47) (2021-07-14)

**Note:** Version bump only for package @psky/system-integration





## [0.0.46](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.45...@psky/system-integration@0.0.46) (2021-07-14)

**Note:** Version bump only for package @psky/system-integration





## [0.0.45](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.44...@psky/system-integration@0.0.45) (2021-07-14)

**Note:** Version bump only for package @psky/system-integration





## [0.0.44](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.43...@psky/system-integration@0.0.44) (2021-07-14)

**Note:** Version bump only for package @psky/system-integration





## [0.0.43](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.42...@psky/system-integration@0.0.43) (2021-07-12)

**Note:** Version bump only for package @psky/system-integration





## [0.0.42](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.41...@psky/system-integration@0.0.42) (2021-07-12)


### Bug Fixes

* main under dist ([876c0f7](https://bitbucket.org/melinmedical/integration-js/commits/876c0f7e190c481cbe8c24c96bd21ef4cc583bdf))





## [0.0.41](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.40...@psky/system-integration@0.0.41) (2021-07-12)

**Note:** Version bump only for package @psky/system-integration





## [0.0.40](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.39...@psky/system-integration@0.0.40) (2021-07-12)


### Bug Fixes

* adjust spaces ([172971f](https://bitbucket.org/melinmedical/integration-js/commits/172971f100a4a6f21d8046583c48a2d37d348d59))





## [0.0.39](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.38...@psky/system-integration@0.0.39) (2021-07-09)


### Bug Fixes

* base url ([ede2861](https://bitbucket.org/melinmedical/integration-js/commits/ede2861c8ef28ae72968888c19f836624b7c5ca1))





## [0.0.38](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.37...@psky/system-integration@0.0.38) (2021-07-09)

**Note:** Version bump only for package @psky/system-integration





## [0.0.37](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.36...@psky/system-integration@0.0.37) (2021-07-08)

**Note:** Version bump only for package @psky/system-integration





## [0.0.36](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.35...@psky/system-integration@0.0.36) (2021-07-08)

**Note:** Version bump only for package @psky/system-integration





## [0.0.35](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.34...@psky/system-integration@0.0.35) (2021-07-08)


### Bug Fixes

* builds ([f06da47](https://bitbucket.org/melinmedical/integration-js/commits/f06da47a9d0f9fc0f65bee1cf1cf2348c13173c7))





## [0.0.34](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.33...@psky/system-integration@0.0.34) (2021-07-08)

**Note:** Version bump only for package @psky/system-integration





## [0.0.33](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.32...@psky/system-integration@0.0.33) (2021-07-08)

**Note:** Version bump only for package @psky/system-integration





## [0.0.32](https://bitbucket.org/melinmedical/integration-js/compare/@psky/system-integration@0.0.31...@psky/system-integration@0.0.32) (2021-07-05)


### Bug Fixes

* test ([d991f86](https://bitbucket.org/melinmedical/integration-js/commits/d991f861aecbf2ba14e3becda425de710911747d))





## 0.0.31 (2021-07-05)

**Note:** Version bump only for package @psky/system-integration
