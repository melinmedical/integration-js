import { config } from "../src/config";
import { HttpBaseContext } from "../src/Context/Context";
import { Context } from "../src/Context/types";
import { HttpSession } from "../src/HttpSession";
import { sessionFactory } from "../src/sessionFactory";
import { RequestResponseFeature } from "../src/SystemFeature/AvailableSystemFeatures";

describe("context", () => {
  let sessionRefrehToken: string;
  let session: HttpSession;
  let context: HttpBaseContext;

  const TENANT_ID = "442ba2fa-ebaa-4136-a54b-77fe0a959b95";

  beforeAll(async () => {
    sessionFactory.initialize({
      partnerGatewayUrl: "https://development-partnergateway.gel.camp",
      onSessionExpired: () => {
        console.log("session has expired");
      },
      getSessionRefreshToken: () => {
        return sessionRefrehToken;
      },
    });
    config.initialize({
      partnerGatewayUrl: "https://development-partnergateway.gel.camp",
    });

    session = await sessionFactory.authenticate(
      "mg4ljXR8KDqlYMC",
      "_jIAp5ZK*hJ080g",
      (refreshToken) => {
        console.log("sessionRefreshToken", refreshToken);
        sessionRefrehToken = refreshToken;
      }
    );
    context = await session.createApplicationContext(TENANT_ID);
  });

  it("can get service categories", async () => {
    const requestResponseFeature = new RequestResponseFeature(
      "global.read.serviceCategories.v1.all",
      "",
      [Context.Application]
    );
    const resp = await requestResponseFeature.request(context, {});
    expect(resp.success).toEqual(true);
  });
});
