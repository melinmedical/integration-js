import { ICarriers } from "../src/Context/Carriers";
import { Context } from "../src/Context/types";
import { HttpContextMock } from "../src/Mock/HttpContextMock";
import { HttpSessionMock } from "../src/Mock/HttpSessionMock";
import { PlatformMock } from "../src/Mock/PlatformMock";
import { Response } from "../src/Response";
import { RequestResponseFeature } from "../src/SystemFeature/AvailableSystemFeatures";

type Input = {
  input: string;
};

type Output = {
  output: string;
};

describe("mock", () => {
  let session: HttpSessionMock;
  let context: HttpContextMock;

  const TENANT_ID = "442ba2fa-ebaa-4136-a54b-77fe0a959b95";

  beforeAll(async () => {
    session = PlatformMock.new()
      .withSystemFeature("testSystemFeature", [
        {
          context: Context.Application,
          handler: (data: Input) => {
            return new Response({ output: `output is: ${data.input}` });
          },
        },
      ])
      .withSystemFeature("testSystemFeatureWithCarriers", [
        {
          context: Context.Application,
          handler: (data: Input, carriers: ICarriers) => {
            return new Response({
              output: `output is: ${data.input}, consumerId: ${carriers.platformConsumer.id}`,
            });
          },
        },
      ])
      .create();

    context = await session.createApplicationContext(TENANT_ID);
  });

  it("can run mock system feature", async () => {
    const requestResponseFeature = new RequestResponseFeature<Input, Output>(
      "testSystemFeature",
      "",
      [Context.Application]
    );

    const input = "surreal";
    const resp = await requestResponseFeature.request(context, {
      body: { input },
    });
    expect(resp.value.output).toEqual(`output is: ${input}`);
  });

  it("can run mock system feature with carriers", async () => {
    const contextWithPlatformConsumerCarrier = context.withConsumer("123");
    const requestResponseFeature = new RequestResponseFeature<Input, Output>(
      "testSystemFeatureWithCarriers",
      "",
      [Context.Application]
    );
    const input = "surreal";
    const resp = await requestResponseFeature.request(
      contextWithPlatformConsumerCarrier,
      {
        body: { input },
      }
    );
    expect(resp.value.output).toEqual(`output is: ${input}, consumerId: 123`);
  });
});
