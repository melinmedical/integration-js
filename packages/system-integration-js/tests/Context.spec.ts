import { HttpSession } from "../src/HttpSession";
import { sessionFactory } from "../src/sessionFactory";

describe("context", () => {
  let sessionRefrehToken: string;
  let session: HttpSession;
  const TENANT_ID = "442ba2fa-ebaa-4136-a54b-77fe0a959b95";
  const healthcareConsumerId = "0499a6e2-3994-11eb-a377-ae057ba76541";

  beforeAll(async () => {
    sessionFactory.initialize({
      partnerGatewayUrl: "https://development-partnergateway.gel.camp",
      onSessionExpired: () => {
        console.log("session has expired");
      },
      getSessionRefreshToken: () => {
        return sessionRefrehToken;
      },
    });

    session = await sessionFactory.authenticate(
      "mg4ljXR8KDqlYMC",
      "_jIAp5ZK*hJ080g",
      (refreshToken) => {
        console.log("sessionRefreshToken", refreshToken);
        sessionRefrehToken = refreshToken;
      }
    );
  });

  it("can get application context", async () => {
    const context = await session.createApplicationContext(TENANT_ID);

    expect(context.contextTokens).toHaveProperty("contextToken");
    expect(context.contextTokens).toHaveProperty("contextTokenTimeout");
  });

  it("can get healthcare consumer context", async () => {
    const context = await session.createHealthcareConsumerContext(
      TENANT_ID,
      healthcareConsumerId
    );
    expect(context.contextTokens).toHaveProperty("contextToken");
    expect(context.contextTokens).toHaveProperty("contextTokenTimeout");
    expect(context.healthcareConsumerId).toEqual(healthcareConsumerId);
  });
});
