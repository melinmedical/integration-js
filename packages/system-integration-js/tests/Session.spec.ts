import { sessionFactory } from "../src/sessionFactory";

describe("session", () => {
  let sessionRefrehToken: string;

  beforeAll(() => {
    sessionFactory.initialize({
      partnerGatewayUrl: "https://development-partnergateway.gel.camp",
      onSessionExpired: () => {
        console.log("session has expired");
      },
      getSessionRefreshToken: () => {
        return sessionRefrehToken;
      },
    });
  });

  it("can authenticate", async () => {
    const session = await sessionFactory.authenticate(
      "mg4ljXR8KDqlYMC",
      "_jIAp5ZK*hJ080g",
      (refreshToken) => {
        console.log("sessionRefreshToken", refreshToken);
        sessionRefrehToken = refreshToken;
      }
    );

    expect(session.sessionTokens).toHaveProperty("sessionToken");
    expect(session.sessionTokens).toHaveProperty("sessionTokenTimeout");

    expect(session.sessionTokens).not.toHaveProperty("sessionRefreshToken");
    expect(sessionRefrehToken).not.toBeNull();
  });
});
