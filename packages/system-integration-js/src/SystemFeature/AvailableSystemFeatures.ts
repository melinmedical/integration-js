import axios, { AxiosResponse } from "axios";

import { config } from "../config";
import { HttpBaseContext } from "../Context/Context";
import { Context } from "../Context/types";
import { isMockContext, HttpContextMock } from "../Mock/HttpContextMock";
import { Response } from "../Response";
import { sessionFactory } from "../sessionFactory";

export class SystemFeature<T, V> {
  private _id: string;
  private _path: string;
  private _availableContexts: Context[];

  public get id() {
    return this._id;
  }
  public get path() {
    return this._path;
  }
  public get availableContexts() {
    return this._availableContexts;
  }

  constructor(id: string, path: string, availableContexts: Context[]) {
    this._id = id;
    this._path = path;
    this._availableContexts = availableContexts;
  }

  public canCall = (context: HttpBaseContext) => {
    if (!context) {
      throw new Error("missing context.");
    }
    return context.availableEndpoints.hasPublishEndpoint(this._id);
  };

  public request = async (
    context: HttpBaseContext | HttpContextMock,
    opts: {
      locale?: string;
      body?: T;
    }
  ): Promise<Response<V>> => {
    if (!context) {
      throw new Error("missing context.");
    }

    if (isMockContext(context)) {
      return context.getSystemFeatureMockResult(
        this.id,
        opts.body as Record<string, unknown>
      );
    }

    const res: AxiosResponse<Response<V>> = await axios.post<
      T,
      AxiosResponse<Response<V>>
    >(
      "module-api/v1/system-feature/request",
      {
        contextToken: context.contextTokens.contextToken,
        id: this._id,
        // locale: "nb-no",
        body: opts.body,
        carriers: context.carriers,
      },
      { baseURL: config.partnerGatewayUrl }
    );

    return this.handleReturn(res.data, context, opts);
  };

  public send = async (
    context: HttpBaseContext,
    opts: { locale?: string; body?: T }
  ): Promise<Response<V>> => {
    if (!context) {
      throw new Error("missing context. have you forgotten to set it?");
    }
    const res: AxiosResponse<Response<V>> = await axios.post<
      T,
      AxiosResponse<Response<V>>
    >(
      "module-api/v1/system-feature/send",
      {
        contextToken: context.contextTokens.contextToken,
        id: this._id,
        body: opts.body,
        carriers: context.carriers,
      },
      { baseURL: config.partnerGatewayUrl }
    );

    return this.handleReturn(res.data, context, opts);
  };

  private handleReturn = async (
    res: Response<V>,
    context: HttpBaseContext,
    opts: { locale?: string; body?: T }
  ) => {
    if (res?.error?.code === "sdk.session.context.tokenExpired") {
      context = await context.refresh();
      return this.request(context, opts);
    } else if (res?.error?.code === "sdk.session.context.refreshTokenExpired") {
      await context.rebuild();
      return this.request(context, opts);
    } else if (res?.error?.code === "sdk.session.tokenExpired") {
      const sessionRefreshToken =
        sessionFactory.config.getSessionRefreshToken?.();
      if (!sessionRefreshToken) {
        throw new Error("session refresh token not found");
      }
      context.session.refresh(sessionRefreshToken);
      await context.rebuild();
      return this.request(context, opts);
    } else if (res?.error?.code === "sdk.session.refreshTokenExpired") {
      sessionFactory.config.onSessionExpired?.();
    }
    return res;
  };
}

export class RequestResponseFeature<T, V> extends SystemFeature<T, V> {}

export class MessageFeature<T> extends SystemFeature<T, void> {}
