interface IAvailableEndpointContract extends IAvailableEndpoint {
  owner: Owner;
  featureId: string;
}

type Owner = {
  id: string;
  type: OwnerType;
};

type OwnerType =
  | "SERVICE"
  | "INTEGRATION"
  | "UNREGISTERED_INTEGRATION"
  | "INTEGRATION_PLUGIN";

export interface IAvailableEndpointsContract {
  publish: IAvailableEndpointContract[];
  subscribe: IAvailableEndpointContract[];
}

export class AvailableEndpoints {
  private availableSubscribe: IAvailableEndpoint[];
  private availablePublish: IAvailableEndpoint[];

  constructor(publish: IAvailableEndpoint[], subscribe: IAvailableEndpoint[]) {
    this.availablePublish = publish;
    this.availableSubscribe = subscribe;
  }

  public get subscribeEndpoints() {
    return this.availableSubscribe;
  }

  public get publishEndpoints() {
    return this.availablePublish;
  }

  public hasPublishEndpoint = (id: string) => {
    return this.availablePublish.some((p) => p.id === id);
  };

  public static from(availableEndpointsContract: IAvailableEndpointsContract) {
    return new AvailableEndpoints(
      availableEndpointsContract.publish.map((p) => ({
        id: p.id,
        handlerType: p.handlerType,
        topic: p.topic,
        qos: p.qos,
      })),

      availableEndpointsContract.subscribe.map((p) => ({
        id: p.id,
        handlerType: p.handlerType,
        topic: p.topic,
        qos: p.qos,
      }))
    );
  }
}

export interface IAvailableEndpoint {
  id: string;
  handlerType: "http" | "mqtt";
  topic: string;
  qos: number;
}
