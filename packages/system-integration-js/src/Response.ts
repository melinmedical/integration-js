export interface IError {
  code: string;
  technicalDescription: string;
  description: string;
}

export class Response<T> {
  private _value: T;
  private _error: IError;
  private _success: boolean;
  private _statusCode: number;

  constructor(value: T) {
    this._value = value;
  }

  get value() {
    return this._value;
  }

  get error() {
    return this._error;
  }

  get success() {
    return this._success;
  }

  get statusCode() {
    return this._statusCode;
  }
}
