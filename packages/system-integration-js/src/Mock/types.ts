import { ICarriers } from "../Context/Carriers";
import { Context } from "../Context/types";
import { Response } from "../Response";

export type HandlerMockWithContext = {
  context: Context;
  handler: (
    body: Record<string, unknown>,
    carriers?: ICarriers
  ) => Response<any>;
};
export type SystemFeatureMock = {
  id: string;
  handlers: HandlerMockWithContext[];
};
