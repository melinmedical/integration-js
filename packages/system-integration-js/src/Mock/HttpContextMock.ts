import { carrierService, ICarriers } from "../Context/Carriers";
import { Context, StringOrIdentifier } from "../Context/types";
import { IPlatformIdentifier, isStringArray, capitalize } from "../types";
import { HttpSessionMock } from "./HttpSessionMock";

class HttpContextMock {
  private sessionMock: HttpSessionMock;
  public carriers: ICarriers = {};
  private type: Context;

  constructor(
    sessionMock: HttpSessionMock,
    type: Context,
    carriers?: ICarriers
  ) {
    this.sessionMock = sessionMock;
    this.type = type;
    this.carriers = carriers;
  }

  public getSystemFeatureMockResult = (
    id: string,
    data: Record<string, unknown>
  ) => {
    const systemFeatureMock = this.sessionMock.systemFeatures.find(
      (sf) => sf.id === id
    );
    return systemFeatureMock.handlers
      .find((h) => h.context === this.type)
      .handler(data, this.carriers);
  };

  private buildCarriers = (data: StringOrIdentifier, carrierType: string) => {
    const carriers: any = { ...this.carriers };
    if (Array.isArray(data)) {
      if (isStringArray(data)) {
        carriers["platform" + capitalize(carrierType) + "s"] = data.map(
          (carrierService as any)["platform" + capitalize(carrierType)]
        );
      } else {
        carriers[carrierType + "s"] = data;
      }
    } else {
      if (typeof data === "string") {
        carriers["platform" + capitalize(carrierType)] = (
          carrierService as any
        )["platform" + capitalize(carrierType)](data);
      } else {
        carriers[carrierType] = data;
      }
    }
    return carriers;
  };

  public withConsumer(data: StringOrIdentifier) {
    return new HttpContextMock(
      this.sessionMock,
      this.type,
      this.buildCarriers(data, "consumer")
    );
  }

  public withProvider(data: StringOrIdentifier) {
    return new HttpContextMock(
      this.sessionMock,
      this.type,
      this.buildCarriers(data, "provider")
    );
  }

  public withPractitioner(data: StringOrIdentifier) {
    return new HttpContextMock(
      this.sessionMock,
      this.type,
      this.buildCarriers(data, "practitioner")
    );
  }

  public withEmployee(data: StringOrIdentifier) {
    return new HttpContextMock(
      this.sessionMock,
      this.type,
      this.buildCarriers(data, "employee")
    );
  }

  public withPosition(data: StringOrIdentifier) {
    return new HttpContextMock(
      this.sessionMock,
      this.type,
      this.buildCarriers(data, "position")
    );
  }

  public addCustom(data: {
    id: string;
    terminology?: string;
    automaticFilter?: boolean;
    automaticWrite: boolean;
  }) {
    const carriers = { ...this.carriers };
    if (!carriers.customCarriers) {
      carriers.customCarriers = [];
    }
    if (Array.isArray(data)) {
      const custom = carriers.customCarriers.concat(
        data.map(carrierService.custom)
      );
      carriers.customCarriers = custom;
    } else {
      carriers.customCarriers.push(carrierService.custom(data));
    }
  }
}

export { HttpContextMock };

export function isMockContext(context: any): context is HttpContextMock {
  return !!context.sessionMock;
}
