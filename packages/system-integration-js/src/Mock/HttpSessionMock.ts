import { Context } from "../Context/types";
import { HttpContextMock } from "./HttpContextMock";
import { SystemFeatureMock } from "./types";

class HttpSessionMock {
  public systemFeatures: SystemFeatureMock[] = [];

  constructor(systemFeatures: SystemFeatureMock[]) {
    this.systemFeatures = systemFeatures;
  }

  public createApplicationContext = async (tenantId: string) => {
    return new HttpContextMock(this, Context.Application);
  };

  public createBackofficeUserContext = async (
    tenantId: string,
    userId: string
  ) => {
    return new HttpContextMock(this, Context.BackOffice);
  };

  public createEmployeeContext = async (
    tenantId: string,
    employeeId: string
  ) => {
    return new HttpContextMock(this, Context.Employee);
  };

  public createHealthcareConsumerContext = async (
    tenantId: string,
    healthcareConsumerId: string
  ) => {
    return new HttpContextMock(this, Context.HealthcareConsumer);
  };

  public createServicePractitionerContext = async (
    tenantId: string,
    servicePractitionerId: string
  ) => {
    return new HttpContextMock(this, Context.ServicePractitioner);
  };

  public createServiceProviderContext = async (
    tenantId: string,
    serviceProviderId: string
  ) => {
    return new HttpContextMock(this, Context.ServiceProvider);
  };
}

export { HttpSessionMock };
