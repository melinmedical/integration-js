import { HttpSessionMock } from "./HttpSessionMock";
import { HandlerMockWithContext, SystemFeatureMock } from "./types";

class PlatformMock {
  public systemFeatures: SystemFeatureMock[] = [];

  public static new = () => {
    return new PlatformMock();
  };

  public withSystemFeature = (
    id: string,
    handlers: HandlerMockWithContext[]
  ) => {
    this.systemFeatures.push({ id, handlers });
    return this;
  };

  public create = () => {
    return new HttpSessionMock(this.systemFeatures);
  };
}

export { PlatformMock };
