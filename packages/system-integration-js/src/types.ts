export interface IPlatformIdentifier {
  id: string;
  terminology: string;
}

export interface ICredentials {
  username: string;
  password: string;
}

export const isStringArray = (
  array: (string | IPlatformIdentifier)[]
): array is string[] => {
  return array.every((item) => {
    return typeof item === "string";
  });
};

export const capitalize = (word: string) => {
  return word[0].toUpperCase() + word.substr(1);
};
