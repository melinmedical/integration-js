import axios, { AxiosRequestConfig } from "axios";
import { HttpClient } from "./HttpClient";
import { HttpSession } from "./HttpSession";
import { Response } from "./Response";
import { IAvailableEndpoint } from "./SystemFeature/AvailableEndpoint";
import { ICredentials } from "./types";

interface IConfig {
  partnerGatewayUrl: string;
  onSessionExpired?: () => void;
  getSessionRefreshToken?: () => null | string;
}

export interface IAuthenticateRequest {
  username: string;
  password: string;
}

export interface ISessionToken {
  sessionToken: string;
  sessionTokenTimeout: string;
}

export interface IAuthenticateResponse extends ISessionToken {
  sessionRefreshToken: string;
  sessionRefreshTokenTimeout: string;
  availableSubscriptions: IAvailableEndpoint[];
}

class SessionFactory {
  private _config: IConfig;
  private partnerGatewayAxiosConfig: AxiosRequestConfig;

  public get config() {
    return this._config;
  }

  public initialize = (config: IConfig) => {
    this._config = config;
    this.partnerGatewayAxiosConfig = {
      baseURL: this._config.partnerGatewayUrl,
    };
  };

  public authenticate = async (
    username: string,
    password: string,
    onSessionRefreshTokenCreated?: (sessionRefreshToken: string) => void
  ) => {
    const response = await axios.post<Response<IAuthenticateResponse>>(
      "module-api/v1/authorization/direct/authenticate",
      {
        username,
        password,
      },
      this.partnerGatewayAxiosConfig
    );

    const res = response.data;

    if (!res.success) {
      throw new Error("unauthorized access");
    }

    onSessionRefreshTokenCreated?.(res.value.sessionRefreshToken);

    const sessionToken: ISessionToken = {
      sessionToken: res.value.sessionToken,
      sessionTokenTimeout: res.value.sessionRefreshToken,
    };

    return new HttpSession(
      new HttpClient(this.config.partnerGatewayUrl),
      sessionToken
    );
  };

  public authenticateWithSession = async (
    sessionId: string,
    credentials: ICredentials,
    onSessionRefreshTokenCreated?: (sessionRefreshToken: string) => void
  ): Promise<HttpSession> => {
    const response = await axios.post<Response<IAuthenticateResponse>>(
      "module-api/v1/authorization/direct/authenticate-login-session",
      {
        username: credentials.username,
        password: credentials.password,
        sessionId,
      },
      this.partnerGatewayAxiosConfig
    );
    const res = response.data;

    if (!res.success) {
      throw new Error(res.error.technicalDescription);
    }

    onSessionRefreshTokenCreated?.(res.value.sessionRefreshToken);

    return new HttpSession(
      new HttpClient(this.config.partnerGatewayUrl),
      res.value
    );
  };

  public refreshSession = async (refreshToken: string) => {
    const res = await axios.post<Response<IAuthenticateResponse>>(
      "module-api/v1/authorization/direct/refresh-session",
      { refreshToken },
      this.partnerGatewayAxiosConfig
    );
    if (!res.data.success) {
      throw new Error(res.data.error.technicalDescription);
    }

    const session = new HttpSession(
      new HttpClient(this.config.partnerGatewayUrl),
      res.data.value
    );

    return session;
  };
}

const sessionFactory = new SessionFactory();

export { sessionFactory };
