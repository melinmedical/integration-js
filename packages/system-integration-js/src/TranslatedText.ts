export class TranslatedText {
  private translations: Map<string, string> = new Map();

  public addTranslation = (locale: string, text: string) => {
    this.translations.set(locale, text);
  };

  public isTranslatedTo = (locale: string) => {
    return this.translations.has(locale);
  };

  public getTranslation = (locale: string) => {
    if (this.isTranslatedTo(locale)) {
      return this.translations.get(locale);
    }
    return this.getDefaultTranslation();
  };

  public getDefaultTranslation = () => {
    // if (this.isTranslatedTo(platformSettings.defaultLocale)) {
    //   return this.translations.get(platformSettings.defaultLocale);
    // }

    if (this.translations.size > 0) {
      return Array.from(this.translations)[0];
    }

    return null;
  };
}
