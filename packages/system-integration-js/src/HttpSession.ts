import {
  HttpApplicationContext,
  HttpBackofficeUserContext,
  HttpEmployeeContext,
  HttpHealthcareConsumerContext,
  HttpServicePractitonerContext,
  HttpServiceProviderContext,
  IContextInfo,
} from "./Context/Context";

import {
  ApplicationContextResponse,
  BackofficeUserContextResponse,
  Context,
  ContextRequest,
  EmployeeContextResponse,
  HealthcareConsumerContextResponse,
  ICreateContextResponseBase,
  ServicePractitionerContextResponse,
  ServiceProviderContextResponse,
} from "./Context/types";
import { HttpClient } from "./HttpClient";
import { ISessionToken } from "./sessionFactory";

export class HttpSession {
  public client: HttpClient;
  private _sessionTokens: ISessionToken;

  constructor(client: HttpClient, sessionTokens: ISessionToken) {
    this.client = client;
    this._sessionTokens = sessionTokens;
  }

  public get sessionTokens() {
    return this._sessionTokens;
  }

  public setSessionTokens = (sessionTokens: ISessionToken) => {
    this._sessionTokens = sessionTokens;
  };

  public close = async () => {
    const res = await this.client.post<{ sessionToken: string }, void>(
      "module-api/v1/authorization/direct/close",
      { sessionToken: this._sessionTokens.sessionToken }
    );
    if (!res.success) {
      throw new Error(res.error.technicalDescription);
    }
  };

  public refresh = async (refreshToken: string) => {
    const res = await this.client.post<{ refreshToken: string }, ISessionToken>(
      "module-api/v1/authorization/direct/refresh-session",
      {
        refreshToken,
      }
    );
    if (!res.success) {
      throw new Error(res.error.technicalDescription);
    }
    this.setSessionTokens({ ...this.sessionTokens, ...res.value });

    return this;
  };

  public rebuildContext = async (ctx: IContextInfo) => {
    if (!ctx) {
      throw new Error("no context to rebuild from");
    }
    const data = ctx.data as any;
    let context;
    switch (ctx.type) {
      case Context.Application:
        context = await this.createApplicationContext(data.tenantId);
        break;
      case Context.BackOffice:
        context = await this.createBackofficeUserContext(
          data.tenantId,
          data.userId
        );
        break;
      case Context.Employee:
        context = await this.createEmployeeContext(
          data.tenantId,
          data.employeeId
        );
        break;
      case Context.HealthcareConsumer:
        context = await this.createHealthcareConsumerContext(
          data.tenantId,
          data.healthcareConsumerId
        );
        break;
      case Context.ServicePractitioner:
        context = await this.createServicePractitionerContext(
          data.tenantId,
          data.servicePractitionerId
        );
        break;
      case Context.ServiceProvider:
        context = await this.createServiceProviderContext(
          data.tenantId,
          data.serviceProviderId
        );
        break;
      default:
        context = await this.createApplicationContext(data.tenantId);
        break;
    }
    context.setCarriers(ctx.carriers);
    return context;
  };

  public createApplicationContext = async (tenantId: string) => {
    const payload = {
      sessionToken: this.sessionTokens.sessionToken,
      tenantId,
    };
    const resp = await this.requestContext<ApplicationContextResponse>(
      Context.Application,
      payload
    );
    const ctx = new HttpApplicationContext(resp, payload, this);
    return ctx;
  };

  public createBackofficeUserContext = async (
    tenantId: string,
    userId: string
  ) => {
    const payload = {
      sessionToken: this.sessionTokens.sessionToken,
      tenantId,
      userId,
    };
    const resp = await this.requestContext<BackofficeUserContextResponse>(
      Context.BackOffice,
      payload
    );
    const ctx = new HttpBackofficeUserContext(resp, payload, this);
    return ctx;
  };

  public createEmployeeContext = async (
    tenantId: string,
    employeeId: string
  ) => {
    const payload = {
      sessionToken: this.sessionTokens.sessionToken,
      tenantId,
      employeeId,
    };
    const resp = await this.requestContext<EmployeeContextResponse>(
      Context.Employee,
      payload
    );
    const ctx = new HttpEmployeeContext(resp, payload, this);
    return ctx;
  };

  public createHealthcareConsumerContext = async (
    tenantId: string,
    healthcareConsumerId: string
  ) => {
    const payload = {
      sessionToken: this.sessionTokens.sessionToken,
      tenantId,
      healthcareConsumerId,
    };
    const resp = await this.requestContext<HealthcareConsumerContextResponse>(
      Context.HealthcareConsumer,
      payload
    );
    const ctx = new HttpHealthcareConsumerContext(resp, payload, this);
    return ctx;
  };

  public createServicePractitionerContext = async (
    tenantId: string,
    servicePractitionerId: string
  ) => {
    const payload = {
      sessionToken: this.sessionTokens.sessionToken,
      tenantId,
      servicePractitionerId,
    };
    const resp = await this.requestContext<ServicePractitionerContextResponse>(
      Context.ServicePractitioner,
      payload
    );
    const ctx = new HttpServicePractitonerContext(resp, payload, this);
    return ctx;
  };

  public createServiceProviderContext = async (
    tenantId: string,
    serviceProviderId: string
  ) => {
    const payload = {
      sessionToken: this.sessionTokens.sessionToken,
      tenantId,
      serviceProviderId,
    };
    const resp = await this.requestContext<ServiceProviderContextResponse>(
      Context.ServiceProvider,
      payload
    );
    const ctx = new HttpServiceProviderContext(resp, payload, this);
    return ctx;
  };

  private requestContext = async <T extends ICreateContextResponseBase>(
    context: Context,
    body: ContextRequest
  ) => {
    body.sessionToken = this.sessionTokens.sessionToken;
    const response = await this.client.post<ContextRequest, T>(
      "/module-api/v1/authorization/direct/contexts/" + context,
      body
    );
    if (!response.success) {
      throw new Error("unauthorized");
    }
    return response.value;
  };
}
