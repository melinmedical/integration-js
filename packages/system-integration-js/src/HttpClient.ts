import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import { Response } from "./Response";

class HttpClient {
  private axiosConfig: AxiosRequestConfig;

  constructor(baseURL: string, apiKey?: string) {
    this.axiosConfig = {
      baseURL,
    };
  }

  public get = async <V>(url: string) => {
    const resp = await axios.get<Response<V>>(url, this.axiosConfig);
    return resp.data;
  };

  public post = async <T, V>(url: string, payload: T): Promise<Response<V>> => {
    const resp = await axios.post<Response<V>>(url, payload, this.axiosConfig);
    return resp.data;
  };

  public put = async <T, V>(url: string, payload: T): Promise<Response<V>> => {
    const resp = await axios.put<Response<V>>(url, payload, this.axiosConfig);
    return resp.data;
  };

  public del = async (url: string): Promise<Response<void>> => {
    const resp = await axios.delete<Response<void>>(url, this.axiosConfig);
    return resp.data;
  };
}

export { HttpClient };
