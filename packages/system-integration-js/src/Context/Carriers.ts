import { IPlatformIdentifier, isStringArray, capitalize } from "../types";
import {
  HttpApplicationContext,
  HttpBackofficeUserContext,
  HttpBaseContext,
  HttpEmployeeContext,
  HttpHealthcareConsumerContext,
  HttpServicePractitonerContext,
  HttpServiceProviderContext,
} from "./Context";
import {
  BackoffceUserContextRequest,
  BackofficeUserContextResponse,
  Context,
  EmployeeContextRequest,
  EmployeeContextResponse,
  HealthcareConsumerContextRequest,
  HealthcareConsumerContextResponse,
  ICustomCarrierIdentifier,
  ServicePractitionerContextRequest,
  ServicePractitionerContextResponse,
  ServiceProviderContextRequest,
  ServiceProviderContextResponse,
  StringOrIdentifier,
} from "./types";

export interface ICarriers {
  platformConsumer?: IPlatformIdentifier;
  platformConsumers?: IPlatformIdentifier[];
  consumer?: IPlatformIdentifier;
  consumers?: IPlatformIdentifier[];
  platformProvider?: IPlatformIdentifier;
  platformProviders?: IPlatformIdentifier[];
  provider?: IPlatformIdentifier;
  providers?: IPlatformIdentifier[];
  platformPractitioner?: IPlatformIdentifier;
  platformPractitioners?: IPlatformIdentifier[];
  practitioner?: IPlatformIdentifier;
  practitioners?: IPlatformIdentifier[];
  platformEmployee?: IPlatformIdentifier;
  platformEmployees?: IPlatformIdentifier[];
  employee?: IPlatformIdentifier;
  employees?: IPlatformIdentifier[];
  platformPosition?: IPlatformIdentifier;
  platformPositions?: IPlatformIdentifier[];
  position?: IPlatformIdentifier;
  positions?: IPlatformIdentifier[];
  customCarriers?: ICustomCarrier[];
}

export interface ICustomCarrier {
  identifier: IPlatformIdentifier;
  automaticFilter: boolean;
  automaticWrite: boolean;
}

class CarrierService {
  public constructor() {}

  public platformConsumer = (id: string) => {
    return { terminology: "reference.actor.healthCareConsumer", id };
  };

  public platformProvider = (id: string) => {
    return { terminology: "reference.platform.serviceProvider", id };
  };

  public platformEmployee = (id: string) => {
    return { terminology: "reference.actor.serviceProvider.employee", id };
  };

  public platformPractitioner = (id: string) => {
    return { terminology: "reference.actor.servicePractitioner", id };
  };

  public platformPosition = (id: string) => {
    return { terminology: "reference.actor.serviceProvider.position", id };
  };

  public custom = (data: {
    id: string;
    terminology?: string;
    automaticFilter?: boolean;
    automaticWrite?: boolean;
  }) => {
    const { id, terminology, automaticFilter, automaticWrite } = data;
    return {
      identifier: { id, terminology },
      automaticFilter: automaticFilter || false,
      automaticWrite: automaticWrite || false,
    };
  };

  public applyConsumerCarrierToContext = <T extends HttpBaseContext>(
    context: HttpBaseContext,
    data: StringOrIdentifier
  ) => {
    return this.applyCarrierToContext<T>(
      context,
      this.buildCarriers(context, data, "consumer")
    );
  };

  public applyPositionCarrierToContext = <T extends HttpBaseContext>(
    context: HttpBaseContext,
    data: StringOrIdentifier
  ) => {
    return this.applyCarrierToContext<T>(
      context,
      this.buildCarriers(context, data, "position")
    );
  };

  public applyPractitionerCarrierToContext = <T extends HttpBaseContext>(
    context: HttpBaseContext,
    data: StringOrIdentifier
  ) => {
    return this.applyCarrierToContext<T>(
      context,
      this.buildCarriers(context, data, "practitioner")
    );
  };

  public applyProviderCarrierToContext = <T extends HttpBaseContext>(
    context: HttpBaseContext,
    data: StringOrIdentifier
  ) => {
    return this.applyCarrierToContext<T>(
      context,
      this.buildCarriers(context, data, "provider")
    );
  };

  public applyEmployeeCarrierToContext = <T extends HttpBaseContext>(
    context: HttpBaseContext,
    data: StringOrIdentifier
  ) => {
    return this.applyCarrierToContext<T>(
      context,
      this.buildCarriers(context, data, "employee")
    );
  };

  public applyCustomCarrierToContext = <T extends HttpBaseContext>(
    context: HttpBaseContext,
    data: ICustomCarrierIdentifier
  ) => {
    const carriers = { ...context.carriers };
    if (!carriers.customCarriers) {
      carriers.customCarriers = [];
    }
    if (Array.isArray(data)) {
      const custom = carriers.customCarriers.concat(data.map(this.custom));
      carriers.customCarriers = custom;
    } else {
      carriers.customCarriers.push(this.custom(data));
    }

    return this.applyCarrierToContext<T>(context, carriers);
  };

  private buildCarriers = (
    context: HttpBaseContext,
    data?: StringOrIdentifier,
    carrierType?: string
  ) => {
    const carriers: any = { ...context.carriers };
    if (Array.isArray(data)) {
      if (isStringArray(data)) {
        carriers["platform" + capitalize(carrierType) + "s"] = data.map(
          (this as any)["platform" + capitalize(carrierType)]
        );
      } else {
        carriers[carrierType + "s"] = data;
      }
    } else {
      if (typeof data === "string") {
        carriers["platform" + capitalize(carrierType)] = (this as any)[
          "platform" + capitalize(carrierType)
        ](data);
      } else {
        carriers[carrierType] = data;
      }
    }

    return carriers;
  };

  private applyCarrierToContext = <T extends HttpBaseContext>(
    context: HttpBaseContext,
    carriers: ICarriers
  ) => {
    let ctx: HttpBaseContext;

    switch (context.type) {
      case Context.Application:
        ctx = new HttpApplicationContext(
          context.contextResponse as HealthcareConsumerContextResponse,
          context.payload as HealthcareConsumerContextRequest,
          context.session,
          carriers
        );
        break;
      case Context.BackOffice:
        ctx = new HttpBackofficeUserContext(
          context.contextResponse as BackofficeUserContextResponse,
          context.payload as BackoffceUserContextRequest,
          context.session,
          carriers
        );
        break;

      case Context.Employee:
        ctx = new HttpEmployeeContext(
          context.contextResponse as EmployeeContextResponse,
          context.payload as EmployeeContextRequest,
          context.session,
          carriers
        );
        break;

      case Context.HealthcareConsumer:
        ctx = new HttpHealthcareConsumerContext(
          context.contextResponse as HealthcareConsumerContextResponse,
          context.payload as HealthcareConsumerContextRequest,
          context.session,
          carriers
        );
        break;

      case Context.ServicePractitioner:
        ctx = new HttpServicePractitonerContext(
          context.contextResponse as ServicePractitionerContextResponse,
          context.payload as ServicePractitionerContextRequest,
          context.session,
          carriers
        );
        break;

      case Context.ServiceProvider:
        ctx = new HttpServiceProviderContext(
          context.contextResponse as ServiceProviderContextResponse,
          context.payload as ServiceProviderContextRequest,
          context.session,
          carriers
        );
        break;
      default:
        ctx = new HttpApplicationContext(
          context.contextResponse as HealthcareConsumerContextResponse,
          context.payload as HealthcareConsumerContextRequest,
          context.session,
          carriers
        );

        break;
    }

    return ctx as T;
  };
}

const carrierService = new CarrierService();
export { carrierService };
