import {
  AvailableEndpoints,
  IAvailableEndpointsContract,
} from "../SystemFeature/AvailableEndpoint";
import { IPlatformIdentifier } from "../types";

export enum Context {
  Application = "application",
  ServiceProvider = "serviceprovider",
  BackOffice = "backofficeuser",
  ServicePractitioner = "servicepractitioner",
  Employee = "employee",
  HealthcareConsumer = "healthcareconsumer",
  Otp = "otp",
}

export interface IApplicationContext extends IContext {}

export interface IOtpContext extends IContext {}

export interface IBackOfficeUserContext extends IContext {}

export interface IEmployeeContext extends IContext {
  servicePracitionerId: string;
  serviceProviderId: string;
  departmentId: string;
  employeeId: string;
  positionId: string;
}
export interface IHealthcareConsumerContext extends IContext {
  healthcareConsumerId: string;
  mergedHealthcareConsumerIds: string[];
}
export interface IServicePractitionerContext extends IContext {
  servicePracitionerId: string;
}
export interface IServiceProviderContext extends IContext {
  serviceProviderId: string;
}

export interface IContext {
  availableEndpoints: AvailableEndpoints;
  accessContext: IAccessContext;
}

export interface IAccessContext {
  userIdentifier: IPlatformIdentifier;
  accountableIdentifier: IPlatformIdentifier;
  tenantIdentifier: IPlatformIdentifier;
}

export interface ApplicationContextResponse
  extends ICreateContextResponseBase {}

export interface BackofficeUserContextResponse
  extends ICreateContextResponseBase {}

export interface ServicePractitionerContextResponse
  extends ICreateContextResponseBase {
  servicePractitionerId: string;
}
export interface ServiceProviderContextResponse
  extends ICreateContextResponseBase {
  serviceProviderId: string;
}
export interface EmployeeContextResponse extends ICreateContextResponseBase {
  serviceProviderId: string;
  servicePractitionerId: string;
  departmentId: string;
  employeeId: string;
  positionId: string;
}
export interface HealthcareConsumerContextResponse
  extends ICreateContextResponseBase {
  healthcareConsumerId: string;
  mergedHealthcareConsumerIds: string[];
}

export type ContextRequest =
  | ApplicationContextRequest
  | BackoffceUserContextRequest
  | EmployeeContextRequest
  | HealthcareConsumerContextRequest
  | ServicePractitionerContextRequest
  | ServiceProviderContextRequest;

export interface ApplicationContextRequest {
  sessionToken: string;
  tenantId: string;
}
export interface BackoffceUserContextRequest {
  sessionToken: string;
  tenantId: string;
  userId: string;
}
export interface EmployeeContextRequest {
  sessionToken: string;
  tenantId: string;
  employeeId: string;
}
export interface HealthcareConsumerContextRequest {
  sessionToken: string;
  tenantId: string;
  healthcareConsumerId: string;
}
export interface ServicePractitionerContextRequest {
  sessionToken: string;
  tenantId: string;
  servicePractitionerId: string;
}
export interface ServiceProviderContextRequest {
  sessionToken: string;
  tenantId: string;
  serviceProviderId: string;
}

export interface ICreateContextResponseBase extends IContextTokens {
  accessContext: IAccessContext;
  tenantId: string;
  availableEndpoints: IAvailableEndpointsContract;
}

export interface IContextTokens {
  contextToken: string;
  contextTokenTimeout: string;
  contextRefreshToken: string;
  contextRefreshTokenTimeout: string;
}

export interface ICustomCarrierIdentifier extends IPlatformIdentifier {
  automaticWrite?: boolean;
  automaticFilter?: boolean;
}

export type StringOrIdentifier =
  | string
  | IPlatformIdentifier
  | string[]
  | IPlatformIdentifier[];
