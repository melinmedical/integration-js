import { HttpSession } from "../HttpSession";
import { AvailableEndpoints } from "../SystemFeature/AvailableEndpoint";
import { carrierService, ICarriers } from "./Carriers";
import {
  ApplicationContextRequest,
  ApplicationContextResponse,
  BackoffceUserContextRequest,
  BackofficeUserContextResponse,
  Context,
  ContextRequest,
  EmployeeContextRequest,
  EmployeeContextResponse,
  HealthcareConsumerContextRequest,
  HealthcareConsumerContextResponse,
  IAccessContext,
  IContext,
  IContextTokens,
  ICreateContextResponseBase,
  ICustomCarrierIdentifier,
  ServicePractitionerContextRequest,
  ServicePractitionerContextResponse,
  ServiceProviderContextRequest,
  ServiceProviderContextResponse,
  StringOrIdentifier,
} from "./types";

export interface IContextInfo {
  type: Context;
  data:
    | { tenantId: string }
    | { tenantId: string; userId: string }
    | { tenantId: string; employeeId: string }
    | { tenantId: string; healthcareConsumerId: string }
    | { tenantId: string; servicePractitionerId: string }
    | { tenantId: string; serviceProviderId: string };
  carriers: ICarriers;
}

export abstract class HttpBaseContext implements IContext {
  public availableEndpoints: AvailableEndpoints;
  public accessContext: IAccessContext;
  public session: HttpSession;
  public contextTokens: IContextTokens;
  public readonly type: Context;
  /** @internal */
  public payload: ContextRequest;
  public contextResponse: ICreateContextResponseBase;

  protected _carriers: ICarriers;

  get client() {
    return this.session.client;
  }

  get carriers() {
    return this._carriers;
  }

  constructor(
    session: HttpSession,
    contextResponse: ICreateContextResponseBase,
    type: Context,
    payload: ContextRequest,
    carriers: ICarriers = {}
  ) {
    this.type = type;
    this.payload = payload;
    this.session = session;
    this.contextResponse = contextResponse;
    this.accessContext = contextResponse.accessContext;
    this.contextTokens = {
      contextRefreshToken: contextResponse.contextRefreshToken,
      contextToken: contextResponse.contextToken,
      contextRefreshTokenTimeout: contextResponse.contextRefreshTokenTimeout,
      contextTokenTimeout: contextResponse.contextTokenTimeout,
    };
    this.availableEndpoints = AvailableEndpoints.from(
      contextResponse.availableEndpoints
    );

    this._carriers = carriers || {};
  }

  public setCarriers = (carriers: ICarriers) => {
    this._carriers = carriers;
  };

  public rebuild = async () => {
    const response = await this.session.client.post<ContextRequest, any>(
      "/module-api/v1/authorization/direct/contexts/" + this.type,
      { ...this.payload, sessionToken: this.session.sessionTokens.sessionToken }
    );
    if (!response.success) {
      throw new Error(response.error.technicalDescription);
    }

    this.accessContext = response.value.accessContext;
    this.contextTokens = {
      contextToken: response.value.contextToken,
      contextRefreshToken: response.value.contextRefreshToken,
      contextTokenTimeout: response.value.contextTokenTimeout,
      contextRefreshTokenTimeout: response.value.contextRefreshTokenTimeout,
    };
    this.availableEndpoints = AvailableEndpoints.from(
      response.value.availableEndpoints
    );
  };

  public refresh = async () => {
    const response = await this.session.client.post<{}, IContextTokens>(
      "/module-api/v1/authorization/direct/contexts/refresh-context",
      {
        refreshToken: this.contextTokens.contextRefreshToken,
      }
    );

    if (!response.success) {
      throw new Error(response.error?.technicalDescription);
    }

    this.contextTokens = response.value;

    return this;
  };
}

export class HttpApplicationContext extends HttpBaseContext {
  constructor(
    contextResponse: ApplicationContextResponse,
    payload: ApplicationContextRequest,
    session: HttpSession,
    carriers?: ICarriers
  ) {
    super(session, contextResponse, Context.Application, payload, carriers);
  }

  public withConsumer(data: StringOrIdentifier) {
    return carrierService.applyConsumerCarrierToContext<HttpApplicationContext>(
      this,
      data
    );
  }

  public withProvider(data: StringOrIdentifier) {
    return carrierService.applyProviderCarrierToContext<HttpApplicationContext>(
      this,
      data
    );
  }

  public withPractitioner(data: StringOrIdentifier) {
    return carrierService.applyPractitionerCarrierToContext<HttpApplicationContext>(
      this,
      data
    );
  }

  public withEmployee(data: StringOrIdentifier) {
    return carrierService.applyEmployeeCarrierToContext<HttpApplicationContext>(
      this,
      data
    );
  }

  public withPosition(data: StringOrIdentifier) {
    return carrierService.applyPositionCarrierToContext<HttpApplicationContext>(
      this,
      data
    );
  }

  public addCustom(data: ICustomCarrierIdentifier) {
    return carrierService.applyCustomCarrierToContext<HttpApplicationContext>(
      this,
      data
    );
  }
}

export class HttpBackofficeUserContext extends HttpBaseContext {
  constructor(
    contextResponse: BackofficeUserContextResponse,
    payload: BackoffceUserContextRequest,
    session: HttpSession,
    carriers?: ICarriers
  ) {
    super(session, contextResponse, Context.BackOffice, payload, carriers);
  }

  public withConsumer(data: StringOrIdentifier) {
    return carrierService.applyConsumerCarrierToContext<HttpBackofficeUserContext>(
      this,
      data
    );
  }

  public withProvider(data: StringOrIdentifier) {
    return carrierService.applyProviderCarrierToContext<HttpBackofficeUserContext>(
      this,
      data
    );
  }

  public withPractitioner(data: StringOrIdentifier) {
    return carrierService.applyPractitionerCarrierToContext<HttpBackofficeUserContext>(
      this,
      data
    );
  }

  public withEmployee(data: StringOrIdentifier) {
    return carrierService.applyEmployeeCarrierToContext<HttpBackofficeUserContext>(
      this,
      data
    );
  }

  public withPosition(data: StringOrIdentifier) {
    return carrierService.applyPositionCarrierToContext<HttpBackofficeUserContext>(
      this,
      data
    );
  }

  public addCustom(data: ICustomCarrierIdentifier) {
    return carrierService.applyCustomCarrierToContext<HttpBackofficeUserContext>(
      this,
      data
    );
  }
}

export class HttpEmployeeContext extends HttpBaseContext {
  private _servicePractitionerId: string;
  private _serviceProviderId: string;
  private _departmentId: string;
  private _employeeId: string;
  private _positionId: string;

  constructor(
    contextResponse: EmployeeContextResponse,
    payload: EmployeeContextRequest,
    session: HttpSession,
    carriers?: ICarriers
  ) {
    super(session, contextResponse, Context.Employee, payload, carriers);
  }

  get servicePractitionerId() {
    return this._servicePractitionerId;
  }

  get serviceProviderId() {
    return this._serviceProviderId;
  }

  get departmentId() {
    return this._departmentId;
  }

  get employeeId() {
    return this._employeeId;
  }

  get positionId() {
    return this._positionId;
  }

  public withConsumer(data: StringOrIdentifier) {
    return carrierService.applyConsumerCarrierToContext<HttpEmployeeContext>(
      this,
      data
    );
  }

  public addCustom(data: ICustomCarrierIdentifier) {
    return carrierService.applyCustomCarrierToContext<HttpEmployeeContext>(
      this,
      data
    );
  }
}

export class HttpHealthcareConsumerContext extends HttpBaseContext {
  private _healthcareConsumerId: string;
  private _mergedHealthcareConsumerIds: string[];

  constructor(
    contextResponse: HealthcareConsumerContextResponse,
    payload: HealthcareConsumerContextRequest,
    session: HttpSession,
    carriers?: ICarriers
  ) {
    super(
      session,
      contextResponse,
      Context.HealthcareConsumer,
      payload,
      carriers
    );
  }

  get healthcareConsumerId() {
    return this._healthcareConsumerId;
  }

  get mergedHealthcareConsumerIds() {
    return this._mergedHealthcareConsumerIds;
  }

  public withProvider(data: StringOrIdentifier) {
    return carrierService.applyProviderCarrierToContext<HttpHealthcareConsumerContext>(
      this,
      data
    );
  }

  public withPractitioner(data: StringOrIdentifier) {
    return carrierService.applyPractitionerCarrierToContext<HttpHealthcareConsumerContext>(
      this,
      data
    );
  }

  public withEmployee(data: StringOrIdentifier) {
    return carrierService.applyEmployeeCarrierToContext<HttpHealthcareConsumerContext>(
      this,
      data
    );
  }

  public withPosition(data: StringOrIdentifier) {
    return carrierService.applyPositionCarrierToContext<HttpHealthcareConsumerContext>(
      this,
      data
    );
  }

  public addCustom(data: ICustomCarrierIdentifier) {
    return carrierService.applyCustomCarrierToContext<HttpHealthcareConsumerContext>(
      this,
      data
    );
  }
}

export class HttpServicePractitonerContext extends HttpBaseContext {
  private _servicePractitionerId: string;

  constructor(
    contextResponse: ServicePractitionerContextResponse,
    payload: ServicePractitionerContextRequest,
    session: HttpSession,
    carriers?: ICarriers
  ) {
    super(
      session,
      contextResponse,
      Context.ServicePractitioner,
      payload,
      carriers
    );
    this._servicePractitionerId = contextResponse.servicePractitionerId;
  }

  get servicePractitionerId() {
    return this._servicePractitionerId;
  }

  public withConsumer(data: StringOrIdentifier) {
    return carrierService.applyConsumerCarrierToContext<HttpServicePractitonerContext>(
      this,
      data
    );
  }

  public withProvider(data: StringOrIdentifier) {
    return carrierService.applyProviderCarrierToContext<HttpServicePractitonerContext>(
      this,
      data
    );
  }

  public withEmployee(data: StringOrIdentifier) {
    return carrierService.applyEmployeeCarrierToContext<HttpServicePractitonerContext>(
      this,
      data
    );
  }

  public withPosition(data: StringOrIdentifier) {
    return carrierService.applyPositionCarrierToContext<HttpServicePractitonerContext>(
      this,
      data
    );
  }

  public addCustom(data: ICustomCarrierIdentifier) {
    return carrierService.applyCustomCarrierToContext<HttpServicePractitonerContext>(
      this,
      data
    );
  }
}

export class HttpServiceProviderContext extends HttpBaseContext {
  private _serviceProviderId: string;

  constructor(
    contextResponse: ServiceProviderContextResponse,
    payload: ServiceProviderContextRequest,
    session: HttpSession,
    carriers?: ICarriers
  ) {
    super(session, contextResponse, Context.ServiceProvider, payload, carriers);
    this._serviceProviderId = contextResponse.serviceProviderId;
  }

  get serviceProviderId() {
    return this._serviceProviderId;
  }

  public withConsumer(data: StringOrIdentifier) {
    return carrierService.applyConsumerCarrierToContext<HttpServiceProviderContext>(
      this,
      data
    );
  }

  public withPractitioner(data: StringOrIdentifier) {
    return carrierService.applyPractitionerCarrierToContext<HttpServiceProviderContext>(
      this,
      data
    );
  }

  public withEmployee(data: StringOrIdentifier) {
    return carrierService.applyEmployeeCarrierToContext<HttpServiceProviderContext>(
      this,
      data
    );
  }

  public withPosition(data: StringOrIdentifier) {
    return carrierService.applyPositionCarrierToContext<HttpServiceProviderContext>(
      this,
      data
    );
  }

  public addCustom(data: ICustomCarrierIdentifier) {
    return carrierService.applyCustomCarrierToContext<HttpServiceProviderContext>(
      this,
      data
    );
  }
}
