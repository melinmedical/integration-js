interface IConfig {
  partnerGatewayUrl: string;
}
class Config {
  private _partnerGatewayUrl: string;

  public initialize = (conf: IConfig) => {
    this._partnerGatewayUrl = conf.partnerGatewayUrl;
  };

  public get partnerGatewayUrl() {
    return this._partnerGatewayUrl;
  }
}

const config = new Config();
export { config };
