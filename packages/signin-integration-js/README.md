# Implementation guide

In order to use PatientSky sign-in integration, frontend application needs to install the following packages

```
npm i @psky/signin-integration @psky/integration-sdk @psky/system-integration
```

Each package has it's own distinct responsibility:
- `@psky/signin-integration` exposes sign-in functionality
- `@psky/system-integration` has all the logic regarding session, context, and calling system features
- `@psky/integration-sdk` holds the available system features on the platform and input/output types of them

## Sign-in integration


Usage of this sign in component consists of three steps:
1. initializing it
   
```tsx
loginService.initialize({
  postLoginRedirectUrl: "http://localhost:3000/home",
  partnerGatewayUrl: "https://development-partnergateway.gel.camp",
  identityUrl: "https://development-identity.gel.camp",
  loginServiceUrl: "https://development-platform-login-service-fe.gel.camp",
  portalUrl: "https://development-portal.gel.camp",
  loginMode: "servicePractitioner",
  clientId: "cp",
  countryCode: "NO",
  onSessionExpired: () => {
    alert("session has expired");
    window.location.href = "/";
  },
  getSessionRefreshToken: () => {
    return localStorage.getItem("sessionRefreshToken");
  },
});
```
2. Login using the component `<LoginView />`
3. Handling the session returned in the callback url parameters
```tsx
const handle = async () => {
  const params = new URLSearchParams(window.location.search);
  const loginSessionId = params.get("loginSessionId");
  if (loginSessionId) {
    const session = await loginService.handleLogin(
        loginSessionId,
        {
          username: "mg4ljXR8KDqlYMC",
          password: "_jIAp5ZK*hJ080g",
        },
        (sessionRefreshToken: string) => {
          localStorage.setItem("sessionRefreshToken", sessionRefreshToken);
        }
      );
    console.log("session", session);
  }
};
```

## Integration


1. Assuming you already have a session, next thing we need to do is to obtain a context

```tsx
 const zd = await session.createApplicationContext(
        "442ba2fa-ebaa-4136-a54b-77fe0a959b95"
      );
```
and store it inside your application. You'll need it later to perform system feature calls.

If we need to set data carriers it can be done on the context object. Methods for setting carriers start with 'with' and accept platform identifier with an optional terminology. In case it's not provided, it will use default platform terminology.
it returns a new instance of a context object..

```tsx
 const contextTwo = new HttpApplicationContext.withConsumer({
        id: "123-123",
      });
```



2. Perform actual system feature call. First argument is context, second is a payload passed to the system feature.

```tsx
const response = await SystemFeatures.Global.Read. ServiceCategories.V1.all.request(
    context,
    {}
  );
```

System features can be found in `@psky/integration-sdk`
```tsx
import { SystemFeatures } from "@psky/integration-sdk";
```

## Maintaining session between page reloads
After a page reload we lose our session and context, so we need to get them back immediately


1. session can be restored simply by using sesionRefreshToken
```tsx
const session = await sessionFactory.refreshSession(sessionRefreshToken);
```
2. contexts are a bit more tricky. session has `rebuildContext` method that can build context

```tsx
  const context = await this.session.rebuildContext(
        contextInfo
      );
```
where `contextInfo` is of type `IContextInfo`

```tsx
export interface IContextInfo {
  type: Context;
  data:
    | { tenantId: string }
    | { tenantId: string; userId: string }
    | { tenantId: string; employeeId: string }
    | { tenantId: string; healthcareConsumerId: string }
    | { tenantId: string; servicePractitionerId: string }
    | { tenantId: string; serviceProviderId: string };
  carriers: ICarriers;
}
```

