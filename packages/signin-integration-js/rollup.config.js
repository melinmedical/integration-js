import typescript from "rollup-plugin-typescript2";

import pkg from "./package.json";

export default {
  input: "index.ts",
  output: {
    dir: "dist",
    // format: "esm",
    // preserveModules: true,
    // preserveModulesRoot: "src",
    // sourcemap: true,
  },
  external: [
    ...Object.keys(pkg.dependencies || {}),
    ...Object.keys(pkg.peerDependencies || {}),
  ],
  plugins: [
    typescript({
      typescript: require("typescript"),
    }),
  ],
};
