import React, { Component } from "react";
import { loginService } from "./loginService";

interface IProps {}

interface IState extends Readonly<typeof initialState> {}

const initialState = {
  height: 0,
};

class LoginView extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = initialState;
  }

  public render() {
    return (
      <iframe
        id="loginIframe"
        width="100%"
        height={this.state.height}
        frameBorder="0"
        src={this.buildUrl()}
      />
    );
  }

  public componentDidMount() {
    window.addEventListener("message", this.messageHandler, false);
  }

  public componentWillUnmount() {
    window.removeEventListener("message", this.messageHandler, false);
  }

  private buildUrl = () => {
    // const loginDomain = "https://development-portal-fe.gel.camp";
    const loginDomain = loginService.config.postLoginRedirectUrl;
    // const redirectUrl = "https://development-portal-fe.gel.camp";
    const redirectUrl = loginService.config.postLoginRedirectUrl;

    return `${
      loginService.config.loginServiceUrl
    }?identityBaseUrl=${encodeURIComponent(
      loginService.config.identityUrl
    )}&countryCode=${loginService.config.countryCode}&loginMode=${
      loginService.config.loginMode
    }&clientId=${encodeURIComponent(
      loginService.config.clientId
    )}&loginDomain=${encodeURIComponent(
      loginDomain
    )}&redirectUrl=${encodeURIComponent(redirectUrl)}`;

    // return `https://development-platform-login-service-fe.gel.camp/?identityBaseUrl=https%3A%2F%2Fdevelopment-identity.gel.camp&countryCode=no&loginMode=patientApp&clientId=app&loginDomain=https%3A%2F%2Fdevelopment-portal.gel.camp&redirectUrl=https%3A%2F%2Fdevelopment-portal.gel.camp%2Fno%2Flogin%3FFmobile%3D1%26version`;
  };

  private messageHandler = (evt: any) => {
    const data = evt.data;
    switch (data.type) {
      case "resize":
        this.resizeHandler(evt);
        break;
    }
  };

  private resizeHandler = (event: any) => {
    this.setState({
      height: event.data && event.data.height > 0 ? event.data.height : 460,
    });
  };
}

export { LoginView };
