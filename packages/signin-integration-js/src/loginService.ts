import {
  config as sytemIntegrationConfig,
  HttpSession,
  sessionFactory,
} from "@psky/system-integration";
import { ICredentials } from "@psky/system-integration";

import { IConfig } from "./types";

class LoginService {
  private _config: IConfig;

  public get config() {
    return this._config;
  }

  public initialize = (config: IConfig) => {
    this._config = config;

    sytemIntegrationConfig.initialize({
      partnerGatewayUrl: this._config.partnerGatewayUrl,
    });
    sessionFactory.initialize(config);
  };

  public handleLogin = async (
    sessionId: string,
    credentials: ICredentials,
    onSessionRefreshTokenCreated: (sessionRefreshToken: string) => void
  ): Promise<HttpSession> => {
    return sessionFactory.authenticateWithSession(
      sessionId,
      credentials,
      onSessionRefreshTokenCreated
    );
  };
}

const loginService = new LoginService();

export { loginService };
