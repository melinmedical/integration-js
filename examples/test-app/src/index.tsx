import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./Components/App";
import reportWebVitals from "./reportWebVitals";
import { sessionFactory } from "@psky/system-integration";
import { sessionService } from "./Services/sessionService";

export const login = async () => {
  try {
    const sessionRefreshToken = localStorage.getItem("sessionRefreshToken");
    if (!sessionRefreshToken || sessionRefreshToken === "null") {
      localStorage.removeItem("sessionRefreshToken");
      localStorage.removeItem("contexts");
      return;
    }
    const session = await sessionFactory.refreshSession(sessionRefreshToken);
    sessionService.setSession(session);

    const contexts = localStorage.getItem("contexts");
    if (!contexts || contexts === "null") {
      localStorage.removeItem("sessionRefreshToken");
      localStorage.removeItem("contexts");
      return;
    }
    const myContexts = await sessionService.rebuildContexts(
      JSON.parse(contexts)
    );
    myContexts.map((ctx) => {
      sessionService.setContext(ctx.name, ctx.context);
    });
  } catch (e) {
    localStorage.removeItem("sessionRefreshToken");
    localStorage.removeItem("contexts");
    console.error(e);
    window.location.href = "/";
  }
};

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
