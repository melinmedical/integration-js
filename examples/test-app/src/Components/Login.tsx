import { loginService, LoginView } from "@psky/signin-integration";
import { FunctionComponent } from "react";

interface IProps {}

loginService.initialize({
  postLoginRedirectUrl: "http://localhost:3000/home",
  //partnerGatewayUrl: "http://localhost:8080",
  partnerGatewayUrl: "https://development-partnergateway.gel.camp",
  identityUrl: "https://development-identity.gel.camp",
  loginServiceUrl: "https://development-platform-login-service-fe.gel.camp",
  portalUrl: "https://development-portal.gel.camp",
  loginMode: "servicePractitioner",
  clientId: "cp",
  countryCode: "NO",
  onSessionExpired: () => {
    alert("session has expired");
    window.location.href = "/";
  },
  getSessionRefreshToken: () => {
    return localStorage.getItem("sessionRefreshToken");
  },
});

const Login: FunctionComponent<IProps> = (props) => {
  return (
    <div className="App">
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          backgroundColor: "lightskyblue",
          height: "100vh",
        }}
      >
        <div style={{ width: 600 }}>
          <LoginView />
        </div>
      </div>
    </div>
  );
};

export { Login };
