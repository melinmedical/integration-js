import "../App.css";

import { loginService } from "@psky/signin-integration";
import { HttpApplicationContext } from "@psky/system-integration";
import React, { FunctionComponent, useEffect, useState } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { login } from "..";
import { sessionService } from "../Services/sessionService";
import { Home } from "./Home";
import { Login } from "./Login";

const App: FunctionComponent<Record<string, unknown>> = () => {
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    void handle();
  }, []);

  const handle = async () => {
    const params = new URLSearchParams(window.location.search);
    const loginSessionId = params.get("loginSessionId");

    if (loginSessionId) {
      if (!loginSessionId) return;
      const session = await loginService.handleLogin(
        loginSessionId,
        {
          // andrius system integration
          username: "mg4ljXR8KDqlYMC",
          password: "_jIAp5ZK*hJ080g",
        },
        (sessionRefreshToken: string) => {
          localStorage.setItem("sessionRefreshToken", sessionRefreshToken);
        }
      );

      sessionService.setSession(session);

      // cannot acquire application context

      const contextOne = await sessionService.session.createEmployeeContext(
        "442ba2fa-ebaa-4136-a54b-77fe0a959b95",
        "66a6f04a-4e4f-11eb-81cd-82aff18884dc"
      );

      sessionService.setContext("one", contextOne);

      localStorage.setItem(
        "contexts",
        JSON.stringify(sessionService.contextInfo())
      );
      const contextTwo = (
        sessionService.getContext("one") as HttpApplicationContext
      )?.withConsumer({
        id: "123-123",
      });
      sessionService.setContext("two", contextTwo);

      localStorage.setItem(
        "contexts",
        JSON.stringify(sessionService.contextInfo())
      );

      window.location.href = "/home";
    } else {
      await login();
    }
    setLoading(false);
  };
  if (loading) {
    return null;
  }
  return (
    <div>
      <Router>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/home" component={Home} />
        </Switch>
      </Router>
    </div>
  );
};

export default App;
