import { SystemFeatures } from "@psky/integration-sdk";
import { RequestResponseFeature } from "@psky/system-integration";
import { FunctionComponent, useEffect, useState } from "react";

import { sessionService } from "../Services/sessionService";

interface IProps {}

const Home: FunctionComponent<IProps> = (props) => {
  const [count, setCount] = useState<number>(0);
  const [currentResponse, setCurrentResponse] = useState<any>();

  const callSystemFeature = async (id: string, path: string) => {
    try {
      if (!sessionService.session) return;

      // const contextOne = sessionService.getContext("one");
      // if (contextOne) {
      //   const response2 =
      //     await SystemFeatures.Global.Read.ServiceCategories.V1.all.request(
      //       contextOne,
      //       {}
      //     );
      //

      const feature = new RequestResponseFeature<unknown, unknown>(
        id,
        path,
        []
      );

      let response;
      try {
        const contextOne = sessionService.getContext("one");
        if (contextOne) {
          response = await feature.request(contextOne, {});
        }
      } catch (e) {
        console.error(e);
      }

      try {
        const contextTwo = sessionService.getContext("two");

        if (contextTwo) {
          response = await feature.request(contextTwo, {});
        }
      } catch (e) {
        console.error(e);
      }

      if (response) {
        setCurrentResponse(response);
        console.log("data", response.value);
      }
    } catch (e) {
      console.error(e);
    }
  };

  const refreshContext = async () => {
    try {
      const contextOne = sessionService.getContext("one");
      if (contextOne) {
        await contextOne.refresh();
      }
      setCount(count + 1);
    } catch (e) {
      console.error(e);
    }
  };

  const logout = async () => {
    try {
      await sessionService.session.close();
      localStorage.removeItem("contexts");
      localStorage.removeItem("sessionRefreshToken");
      document.location.href = "/";
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <div className="App">
      <div style={{ margin: 20, display: "flex", justifyContent: "right" }}>
        <button onClick={refreshContext}>Refresh context</button>
        <button onClick={logout}>logout</button>
      </div>
      <div
        style={{
          margin: 20,
          display: "flex",
          justifyContent: "left",
          flexDirection: "row",
        }}
      >
        <div
          style={{
            margin: 20,
            width: 400,
            display: "flex",
            justifyContent: "right",
            flexDirection: "column",
          }}
        >
          {sessionService
            .getContext("one")
            ?.availableEndpoints.publishEndpoints.map((endpoint) => {
              return (
                <button
                  key={endpoint.id}
                  onClick={() => callSystemFeature(endpoint.id, endpoint.topic)}
                >
                  {endpoint.id}
                </button>
              );
            })}
          {/* <button onClick={getServiceCategories}>
            SystemFeatures.Global.Read.ServiceCategories.V1.all
          </button> */}
        </div>
        <div>
          <pre style={{ textAlign: "left" }}>
            {JSON.stringify(currentResponse, null, 2)}
          </pre>
        </div>
      </div>
      <div>
        <div>
          <div style={{ display: "flex" }}>
            session
            <button
              onClick={() => {
                setCount(count + 1);
              }}
            >
              update
            </button>
          </div>
          <pre style={{ textAlign: "left" }}>
            {JSON.stringify(sessionService.session?.sessionTokens, null, 2)}
          </pre>
        </div>
        <div>
          <div style={{ display: "flex" }}>
            context
            <button
              onClick={() => {
                setCount(count + 1);
              }}
            >
              update
            </button>
          </div>
          <pre style={{ textAlign: "left" }}>
            {JSON.stringify(
              sessionService.getContext("one")?.contextTokens,
              null,
              2
            )}
          </pre>
        </div>
      </div>
    </div>
  );
};

export { Home };
