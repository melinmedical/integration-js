import {
  HttpBaseContext,
  HttpSession,
  IContextInfo,
} from "@psky/system-integration";

interface IMyContextInfo extends IContextInfo {
  name: string;
}

class SessionService {
  private _session: HttpSession;
  private contexts = new Map<string, HttpBaseContext>();

  public get session() {
    return this._session;
  }

  public setSession = (session: HttpSession) => {
    this._session = session;
  };

  public rebuildContexts = async (contextInfo: IMyContextInfo[]) => {
    const rebuiltContexts: { name: string; context: HttpBaseContext }[] = [];
    for (let i = 0; i < contextInfo.length; i++) {
      const context = await this.session.rebuildContext(
        contextInfo[i] as IContextInfo
      );
      rebuiltContexts.push({
        name: contextInfo[i].name,
        context: context as HttpBaseContext,
      });
    }

    return rebuiltContexts;
  };

  public contextInfo = () => {
    const contextInfo: IMyContextInfo[] = [];
    this.contexts.forEach((value, name) => {
      const v = value as any;
      const data = { ...v.payload };
      data.sessionToken = "";
      contextInfo.push({
        name,
        data: v.payload,
        type: value.type,
        carriers: value.carriers,
      });
    });
    return contextInfo;
  };

  public setContext = (name: string, context: HttpBaseContext) => {
    this.contexts.set(name, context);
  };

  public getContext = (name: string) => {
    return this.contexts.get(name);
  };
}

const sessionService = new SessionService();
export { sessionService };
