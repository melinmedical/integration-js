/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import {Platform, SystemFeatures} from '@psky/integration-sdk';
import {loginService, LoginView} from '@psky/signin-integration-rn';
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import {
  Colors,
  Header,
  LearnMoreLinks,
} from 'react-native/Libraries/NewAppScreen';

const Section = ({children, title}) => {
  const isDarkMode = useColorScheme() === 'dark';
  const [result, setResult] = useState<Platform.ServiceCategory[]>(null);
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      {loginService.config && !result ? (
        <LoginView
          onError={err => {
            console.log(err);
          }}
          onSuccess={async sessionId => {
            const session = await loginService.handleLogin(
              sessionId,
              {
                username: 'mg4ljXR8KDqlYMC',
                password: '_jIAp5ZK*hJ080g',
              },
              sessionRefreshToken => {
                console.log('sessionRefreshToken', sessionRefreshToken);
                // AsyncStorage.setItem(
                //   'sessionRefreshToken',
                //   sessionRefreshToken,
                // );
              },
            );

            const context = await session.createHealthcareConsumerContext(
              '442ba2fa-ebaa-4136-a54b-77fe0a959b95',
              '0499a6e2-3994-11eb-a377-ae057ba76541',
            );

            const result =
              await SystemFeatures.Global.Read.ServiceCategories.V1.all.request(
                context,
                {},
              );

            setResult(result.value);
          }}
        />
      ) : (
        <Text>{result ? JSON.stringify(result, null, 2) : 'Loading'}</Text>
      )}

      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};

const App = () => {
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    loginService.initialize({
      // partnerGatewayUrl: 'http://localhost:8080',
      partnerGatewayUrl: 'https://development-partnergateway.gel.camp',
      identityUrl: 'https://development-identity.gel.camp',
      loginServiceUrl: 'https://development-platform-login-service-fe.gel.camp',
      portalUrl: 'https://development-portal.gel.camp',
      loginMode: 'patientApp',
      clientId: 'app',
      countryCode: 'no',
      onSessionExpired: () => {
        alert('session has expired');
        window.location.href = '/';
      },
      getSessionRefreshToken: async () => {
        await AsyncStorage.getItem('sessionRefreshToken');
      },
    });

    setLoading(false);
  }, []);

  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <Header />
        <View
          style={{
            backgroundColor: isDarkMode ? Colors.black : Colors.white,
          }}>
          {!loading && (
            <Section title="Step One">
              Edit <Text style={styles.highlight}>App.js</Text> to change this
              screen and then come back to see your edits.
            </Section>
          )}

          <LearnMoreLinks />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
